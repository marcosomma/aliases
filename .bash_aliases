########################################################################################
#########################
#########################     GIT
#########################
########################################################################################
alias g='git'
alias gs='git status'
alias gsModified="git ls-files -m"
alias ga='git add'
alias gaUndo='git reset' 
alias gResetLocalChangesMaster='git reset --hard origin/master'
alias gResetLocalChangesDevelop='git reset --hard origin/develop'
alias gCommit='git commit'
alias gCommitAfterMergeWithNoFiles='git commit'
alias gClone='git clone'
alias gBranchNameCurrent='git rev-parse --symbolic-full-name --abbrev-ref HEAD'

################################## stash'ing
alias gStash='git stash'

################################# git stash show in diff style
alias gStashShow='git stash show -p'
alias gStashPop='git stash pop'
alias gStashDrop='git stash drop'

################################## incoming stat
alias gIncomingMasterStat='git fetch && git log ..origin/master --stat'
alias gIncomingDevelopStat='git fetch && git log ..origin/develop --stat'
alias gOutgoingDevelopStat='git fetch && git log origin/develop.. --stat'
alias gOutgoingMasterStat='git log --pretty=oneline --abbrev-commit --graph @{u}.. --stat'
alias gWhatChangedMasterStat='git whatchanged origin/master.. --stat'
alias gWhatChangedDevelopStat='git whatchanged origin/develop.. --stat'

################################## incoming patch
alias gIncomingMasterPatch='git fetch && git log ..origin/master --patch'
alias gIncomingDevelopPatch='git fetch && git log ..origin/develop --patch'

################################## analyse commit
alias gShow='git show'
alias gPushMaster='git push origin master -v'
alias gPushUMaster='git push -u origin master'
alias gPushStaging='git push origin staging -v'
alias gPushUStaging='git push -u origin master'
alias gPushDevelop='git push origin staging -v'
alias gPushUDevelop='git push -u origin develop'
alias gPushCurrentBranch='git push origin "$(git rev-parse --symbolic-full-name HEAD)" -v'
alias gPullMaster='git pull origin master -v'
alias gPullDevelop='git pull origin develop -v'
alias gPullCurrentBranch='git pull origin "$(git rev-parse --symbolic-full-name HEAD)" -v'
alias gPullAllBranches='git pull'
alias gDiff='git diff --word-diff="color"'
alias gCheckout='git checkout'
alias gCheckoutHEAD='git checkout HEAD^1'

################################# branching
alias gBranchesLocal='git branch'
alias gBranchesRemote='git branch -r'
alias gSwitchBranch='git checkout'
alias gSwitchBranchToMaster='git checkout master'
alias gCreateLocalBranch='git checkout -b'
alias gPushLocalBranch='git push -u origin'
alias gDeleteLocalBranch='git branch -d' 
alias gDeleteRemoteBranch='git push origin --delete'  
alias gRemoteUpdate='git remote update'
alias gMergeBranchWithLatestChanges='git pull && git merge'
alias gConflictedFiles='git diff --name-only --diff-filter=U'

################################# update your fork
alias gRemoteAddUpstream='git remote add upstream'
alias gRemoteAddOrigin='git remote add origin'
alias gMergeChildBranch='git merge' 
alias gRebaseFromParentBranch='git rebase' 
alias gBranchesCommiterDateMessages='git branch -vv'
alias gBranchesCommiterDate='git for-each-ref --sort=-committerdate refs/heads/'
alias gBranchesCommiterDateFormat='git for-each-ref --sort=-committerdate refs/heads/ --format="%(authordate:short)%09%(objectname:short)%09%1B[0;33m%(refname:short)%1B[m%09"'
alias gBranchesCommiterDatePretty='for ref in $(git for-each-ref --sort=-committerdate --format="%(refname)" refs/heads/ refs/remotes ); do git log -n1 $ref --pretty=format:"%Cgreen%cr%Creset %C(yellow)%d%Creset %C(bold blue)<%an>%Creset%n" | cat ; done | awk '"'! a["'$0'"]++'"
alias gRenameBranch="git branch -m"
alias gRenameCurrentBranch="git branch -m"

################################### log
alias glpd="git log --graph --all --decorate --pretty=format:'%Cred%h%Creset %Cgreen(%ad)%Creset %s %Cblue(%an)%Creset' --date=short"
alias glpdr="git log --graph --pretty=format:'%Cred%h%Creset %an -%C(yellow)%d%Creset %s %Cgreen(%cr)%Creset' --abbrev-commit --date=relative"

################################### refactor
alias gRemove='git rm -r'
alias gCheckoutToSpecificCommit='git checkout -b'

################################### patches
alias gCreatePatch='git format-patch' 
alias gCreatePatcheLast10Commits='git format-patch -10 HEAD --stdout > last-10-commits.patch'
alias gApplyPatches='git apply --stat'

#######################################################################################
#########################
#########################     other personal aliases
#########################
########################################################################################
alias grailsSpockTest='grails test-app unit:spock'

################################### nodejs
alias npmLs='npm ls'
alias n='node'
alias lh='ls -lh'
alias fileSpaceUsage='du --max-depth=1 -h'
alias fileSpaceUsageSort='du -sm * | sort -n | tail -n 20'
alias goMakalu='cd $WORKSPACE/MakaluDevelop/DasTest && git status'
alias letscode='cd $CODE_SPACE'
alias letsmusic='cd $MUSIC_SPACE/METAL && ls -l'
alias androidStudioStart='/usr/local/android-studio/bin/studio.sh >' #/usr/local/android-studio/studio.log &'
alias reboot='sudo reboot'
alias enoughCoding='sudo shutdown -h now'

################################### while sleeping
alias musicalSleep='sudo shutdown -h 60'
alias soundcardCheck='sudo aplay -l'
alias mouseRestart='sudo modprobe -r psmouse && sudo modprobe psmouse'
alias findPort='sudo lsof -iTCP -sTCP:LISTEN | grep' #findPort amqp
alias trash='sudo rm -rf ~/.local/share/Trash/*'
alias dpkgInstall='sudo dpkg -i'
alias diskspaceUsage='df -h'
alias kernelVersion='uname -r'
alias kernelBit='uname -m'
alias checkProxy='env | grep -i proxy'

################################### FILES
alias findFile='find . -name'
alias grepInFiles='grep -r'
alias ip="ifconfig eth0| grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"
alias sshVbox='ssh vmfest@192.168.56.104'
alias publicSSHKey='cat ~/.ssh/id_rsa.pub'

########################################################################################
#########################
######################### MY SETTINGS 
#########################
########################################################################################
alias applicazioni='cd ~/App'
alias documents='cd ~/Documents'
alias downloads='cd ~/Downloads'
alias desktop='cd ~/Desktop'
alias work='cd ~/Documents/workspace/'
alias work-n='cd ~/Documents/workspace/node'
alias work-r='cd ~/Documents/workspace/ruby'
alias work-p='cd ~/Documents/workspace/python'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

#################################### Manage packages
alias agi='apt-get install'
alias sagi='sudo apt-get install'
alias agr='apt-get remove'
alias sagr='sudo apt-get remove'
alias agu='apt-get update'
alias sagu='sudo apt-get update'
alias acs='apt-cache search'

#################################### Manage files
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -i'
alias la='ls -alh'

#################################### Other
alias e='exit'
alias s='sudo'
alias suspend='sudo pm-suspend'
alias lock='gnome-screensaver-command --lock'

#################################### Rails
alias r='rails'

#################################### Bundler
alias bu='bundle'

#################################### Rake
alias ra='rake'

#################################### Ruby
alias ru='ruby'

#################################### Bundler
alias bu='bundler'

#################################### #PYTHON
alias py='python'
alias pyrun='python manage.py runserver'
alias pytest='python manage.py test'
alias pysyncdb='python manage.py syncdb'
alias pyschemamigration='python manage.py schemamigration'
alias pymigrate='python manage.py migrate'
alias pyschemamig='python manage.py schemamigration'

#################################### HEXO
alias hCln='hexo clean'
alias hCfg='hexo config'
alias hDpl='hexo deploy'
alias hGen='hexo generate'
alias hH='hexo help'
alias hI='hexo init'
alias hLs='hexo list'
alias hMigrate='hexo migrate'
alias hNew='hexo new'
alias hPub='hexo publish'
alias hRender='hexo render'
alias hServer='hexo server'
alias hV='hexo version'

#################################### LOCATIONS
alias workspace='cd ~/Documents/workspace/'

#################################### SYSTEM
alias sshKeyShow='cat ~/.ssh/id_rsa.pub'

#################################### DOCKER
alias runDocker='sudo docker -d'

#################################### AT WORK
alias onworkapp='workon on_dev && work-p && cd opportunitynet/opportunitynet/'
alias onworkdb='workon on_dev && work-p && cd opportunitynet-obfuscated-data'
alias onkoala='workon koala && cd ~/Documenti/personal/python/koala'
alias onkoalacms='workon onkoalacms && work-p && cd on-koala'
